
#ifndef KDE4SUPPORT_EXPORT_WRAPPER_H
#define KDE4SUPPORT_EXPORT_WRAPPER_H

#include "kde4support_export_internal.h"

#ifndef KDE4SUPPORT_NO_DEPRECATED_NOISE
#define KDE4SUPPORT_DEPRECATED_NOISE KDE4SUPPORT_DEPRECATED
#define KDE4SUPPORT_DEPRECATED_EXPORT_NOISE KDE4SUPPORT_DEPRECATED_EXPORT
#else
#define KDE4SUPPORT_DEPRECATED_NOISE
#define KDE4SUPPORT_DEPRECATED_EXPORT_NOISE KDE4SUPPORT_EXPORT
#endif

#endif
