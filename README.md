# KDE4 Support

## Introduction

This framework provides code and utilities to ease the transition from
kdelibs 4 to KDE Frameworks 5.  This includes CMake macros and C++
classes whose functionality has been replaced by code in CMake, Qt and
other frameworks.

Code should aim to port away from this framework eventually.  The API
documentation of the classes in this framework and the notes at
<http://community.kde.org/Frameworks/Porting_Notes> should help with
this.

Note that some of the classes in this framework, especially
KStandardDirs, may not work correctly unless any libraries and other
software using the KDE4 Support framework are installed to the same
location as KDE4Support, although it may be sufficient to set the
KDEDIRS environment variable correctly.

## Links

- Mailing list: <https://mail.kde.org/mailman/listinfo/kde-frameworks-devel>
- IRC channel: #kde-devel on Freenode
- Git repository: <https://projects.kde.org/projects/frameworks/kde4support/repository>
